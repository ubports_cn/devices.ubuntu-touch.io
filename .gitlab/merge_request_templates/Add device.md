# Add device port

Device codename: "codename of your device"

Device repository: "github.com/"

Release channels available: "stable|release candidate|development|edge"

Default release channel: "stable"

Is the device able to boot? "yes|no"

Where can I find image builds? "github.com/"

## Porting and install notes
