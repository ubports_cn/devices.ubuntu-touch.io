import feedGenerator from "@logic/build/generateRss.js";

export async function GET() {
  const feed = await feedGenerator("feed-generate");

  return new Response(feed.rss2());
}
