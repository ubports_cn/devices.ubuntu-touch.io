import facebookLogoImg from "@assets/img/services/facebook.svg";
import gitLogoImg from "@assets/img/services/git.svg";
import githubLogoImg from "@assets/img/services/github.svg";
import gitlabLogoImg from "@assets/img/services/gitlab.svg";
import instagramLogoImg from "@assets/img/services/instagram.svg";
import ircLogoImg from "@assets/img/services/irc.svg";
import linkedinLogoImg from "@assets/img/services/linkedin.svg";
import mastodonLogoImg from "@assets/img/services/mastodon.svg";
import pixelfedLogoImg from "@assets/img/services/pixelfed.svg";
import rssLogoImg from "@assets/img/services/rss.svg";
import soundcloudLogoImg from "@assets/img/services/soundcloud.svg";
import telegramLogoImg from "@assets/img/services/telegram.svg";
import twitterLogoImg from "@assets/img/services/twitter.svg";
import youtubeLogoImg from "@assets/img/services/youtube.svg";
import yumiLogoImg from "@assets/img/services/yumi.svg";

export default () => {
  return {
    facebook: facebookLogoImg,
    git: gitLogoImg,
    github: githubLogoImg,
    gitlab: gitlabLogoImg,
    instagram: instagramLogoImg,
    irc: ircLogoImg,
    linkedin: linkedinLogoImg,
    mastodon: mastodonLogoImg,
    pixelfed: pixelfedLogoImg,
    rss: rssLogoImg,
    soundcloud: soundcloudLogoImg,
    telegram: telegramLogoImg,
    twitter: twitterLogoImg,
    youtube: youtubeLogoImg,
    yumi: yumiLogoImg
  };
};
