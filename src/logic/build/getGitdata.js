import * as path from "path";
import { execFileSync } from "child_process";

export default (filePath) => {
  // Get relative file path for the current file
  let repo = process.cwd();
  let file = path.relative(repo, filePath);

  // Execute git
  const stdout = execFileSync(
    "git",
    ["log", "-n -1", "--pretty=@begin@%H\\t%an\\t%ai@end@", "--", file],
    { cwd: repo }
  ).toString();

  // Parse commits from git log stdout
  return stdout
    .split("@begin@")
    .filter((commit) => !!commit)
    .map((commit) => {
      let commitData = commit.split("@end@")[0].split("\\t");
      return {
        hash: commitData[0],
        authorName: commitData[1],
        // Format git dates to JS dates
        authorDate: new Date(
          commitData[2].replace(/\+(\w{2})(\w{2})/, "+$1:$2")
        )
      };
    });
};
