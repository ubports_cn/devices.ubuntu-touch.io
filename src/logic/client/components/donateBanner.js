class DonateBanner extends HTMLElement {
  constructor() {
    super();

    let noDonationTime = localStorage.getItem("noDonationPopupTime");
    let donateVisible = noDonationTime ? noDonationTime < Date.now() : true;
    if (!donateVisible) this.classList.add("d-none");

    document.getElementById("disableDonation").onclick = () => {
      localStorage.setItem("noDonationPopupTime", Date.now() + 259200000);
      this.classList.add("d-none");
    };
  }
}

export default DonateBanner;
